### Representing a MidiFile as a musical score

This library was designed to be able to statistically analyse large corpora
of MIDI files. The library is based on the ZMidi.Core library: 
https://hackage.haskell.org/package/zmidi-core 
 